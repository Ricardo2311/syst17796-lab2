package sheridan.richie.student;


public class Student {
    
    private String name;
    private String id;
   
    public Student(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    

    public String getId() {
        return id;
    }

    public Student(String name, String id) {
        this.name = name;
        this.id = id;
    }

    

}

